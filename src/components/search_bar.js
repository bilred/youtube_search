import React, { Component } from 'react';

class SearchBar extends Component {

	constructor(props) {
		super(props);

		this.state = { term: '' }; //first value
	}

	render() {
		return (
				<div className="search-bar">
					<input
						value={this.state.term}
						onChange={event => this.onInputChange(event.target.value)}
						/>
						Value of the input: {this.state.term}
				</div>
		);
			//return <input onChange={(event) => console.log(event.target.value)} />;
	}

	//onInputChange(event) {
	//  console.log(event.target.value);
	//}
	onInputChange(term) {
		this.setState({term}); //why use {...} with setStat and term: is special?
		this.props.onSearchTermChange(term);
	}
}

export default SearchBar;
